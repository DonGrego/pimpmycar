// Définition des constantes du carrousel
const carousel = document.querySelector(".carousel");
const items = document.querySelectorAll(".item");
let currdeg  = 0;

/**
 * Cette fonction est utilisée pour le carrousel. Elle vient faire tourner le carrousel en fonction du clic.
 * - Si elle reçoit `n`, elle enlève 60° à la rotation.
 * - Si elle reçoit `p`, elle ajoute 60° à la rotation.
 *
 * Par la suite, elle vient **ajouter** le degré de rotation au `carrousel`.
 *
 * On `parcourt` tous les éléments qui contiennent la class `item` et retire le degré de rotation du carrousel.
 *
 * @param{string} e
 */
function rotate(e){
    // Vérifie si on va en avant ou en arrière
    if(e =="n"){
        currdeg -= 60;
    }
    if(e =="p"){
        currdeg += 60;
    }

    // On donne la rotation au carrousel
    carousel.style.transform = `rotateY(${currdeg}deg)`;

    // Définie le contraire du currentdeg
    let contraryCurrdeg = -currdeg
    // Parcourt tous les items pour retirer le degré de rotation
    console.log(items.length)
    for (let i = 0; i < items.length; i++) {
        items[i].style.transform = `rotateY(${contraryCurrdeg}deg)`;
    }
}